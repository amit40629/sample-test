DECLARE
    TEST VARCHAR(40) := 'Hi Amit Singh';
BEGIN
    DBMS_OUTPUT.PUT_LINE(TEST);
END;


DECLARE
    MESSAGE VARCHAR(50):= 'Hello World';
BEGIN
    DBMS_OUTPUT.PUT_LINE (MESSAGE);
END;
/

DECLARE
    SUBTYPE NAME IS
        CHAR(6);
    SUBTYPE MESSAGE IS
        VARCHAR(60);
    READER     NAME;
    SALUTATION MESSAGE;
BEGIN
    READER := '=';
    SALUTATION:='How are you today';
    DBMS_OUTPUT.PUT_LINE('Hello'
        || ' '
        || READER
        || SALUTATION);
END;

DECLARE
    A INTEGER:= 5;
    B INTEGER:= 10;
    C INTEGER;
BEGIN
    C :=A+B;
    DBMS_OUTPUT.PUT_LINE(CONCAT('value of c:', C));
    DECLARE
        D INT := 10;
        E INT := 10;
        F INT;
    BEGIN
        F:=D/E;
        DBMS_OUTPUT.PUT_LINE('value of F:'
            || ' '
            || F);
    END;
END;


DECLARE
    AMIT  VARCHAR(15);
    SINGH VARCHAR(30);
BEGIN
    AMIT:='Hi this is ';
    SINGH:= 'Amit Kumar';
    DBMS_OUTPUT.PUT_LINE (AMIT || SINGH);
END;


DECLARE
    N NUMBER:=5;
    I NUMBER;
    J NUMBER;
BEGIN
    FOR I IN 1..N LOOP
        FOR J IN 1..I LOOP
            DBMS_OUTPUT.PUT('*');
        END LOOP;
        DBMS_OUTPUT.NEW_LINE;
    END LOOP;
END;
/



create table OrderItem (
   Id                   int                  Not null,
   OrderId              int                  not null,
   ProductId            int                  not null,
   UnitPrice            decimal(12,2)        not null ,
   Quantity             int                  not null ,
   constraint PK_ORDERITEM primary key (Id)
)

INSERT INTO OrderItem (Id,OrderId,ProductId,UnitPrice,Quantity)VALUES(1,1,11,14.00,12);
INSERT INTO OrderItem (Id,OrderId,ProductId,UnitPrice,Quantity)VALUES(2,1,42,9.80,10);
INSERT INTO OrderItem (Id,OrderId,ProductId,UnitPrice,Quantity)VALUES(3,1,72,34.80,5);
INSERT INTO OrderItem (Id,OrderId,ProductId,UnitPrice,Quantity)VALUES(4,2,14,18.60,9);

select * from OrderItem;
select * from Customer;

CREATE TABLE COUNTRIES (
    COUNTRY_ID CHAR( 2 ) PRIMARY KEY,
    COUNTRY_NAME VARCHAR2( 40 ) NOT NULL,
    REGION_ID INTEGER
);

Insert into COUNTRIES (COUNTRY_ID,COUNTRY_NAME,REGION_ID) values ('AR','Argentina',2);
Insert into COUNTRIES (COUNTRY_ID,COUNTRY_NAME,REGION_ID) values ('AU','Australia',3);
Insert into COUNTRIES (COUNTRY_ID,COUNTRY_NAME,REGION_ID) values ('BE','Belgium',1);
Insert into COUNTRIES (COUNTRY_ID,COUNTRY_NAME,REGION_ID) values ('BR','Brazil',2);
Insert into COUNTRIES (COUNTRY_ID,COUNTRY_NAME,REGION_ID) values ('CA','Canada',2);
Insert into COUNTRIES (COUNTRY_ID,COUNTRY_NAME,REGION_ID) values ('CH','Switzerland',1);
Insert into COUNTRIES (COUNTRY_ID,COUNTRY_NAME,REGION_ID) values ('CN','China',3);
Insert into COUNTRIES (COUNTRY_ID,COUNTRY_NAME,REGION_ID) values ('DE','Germany',1);
Insert into COUNTRIES (COUNTRY_ID,COUNTRY_NAME,REGION_ID) values ('DK','Denmark',1);

SELECT * FROM countries


DECLARE
    C_ID   COUNTRIES.COUNTRY_ID%TYPE:='AR';
    C_NAME COUNTRIES.COUNTRY_NAME%TYPE;
BEGIN
    SELECT
        COUNTRY_ID,
        COUNTRY_NAME INTO C_ID,
        C_NAME
    FROM
        COUNTRIES
    WHERE
        COUNTRY_ID ='AR';
    DBMS_OUTPUT.PUT_LINE(C_NAME
        || C_ID);
END;