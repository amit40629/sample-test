select * from amit order by AGENT_NAME;

update amit
SET AGENT_NAME = 'AMIT'
where AGENT_NAME ='Singh'

--DUPLICATE ROW REMOVE QUERY--
with CTE as (
select AGENT_NAME,
ROW_NUMBER() over (partition by AGENT_NAME order by AGENT_NAME) as test
from amit
)
DELETE from CTE
where test > 1


--RANK,DENSE RANK, ROW NUMBER--
with amitCTE as 
(
	select ROW_NUMBER() over(partition by WORKING_AREA order by WORKING_AREA) as ROW_NUMBER --row number function start with 1,2 then 1
	, DENSE_RANK() over( order by WORKING_AREA) as DENSE_RANK -- dense rank function assign same value to duplicates and then 2,3,...
    , RANK() over( order by WORKING_AREA) as RANK --rank function also assign same value to duplicates but then it skip 2 or 3 if duplicate
    ,WORKING_AREA
	 from amit
)
select * from amitCTE

select * from amit

--
CREATE TABLE displayString(
	[Name] [Varchar](60) NOT NULL,
	[Usage] [Varchar](40) NULL,
)
select * from displayString

Insert into displayString values ('Mr. Amit Singh','DatabaseDemoStructuredQueryLanguage')

--Query to display base Singh Mr.
Select Name, CONCAT(SUBSTRING(Usage,5,4),' ' ,SUBSTRING(Name,10,5),' ' , SUBSTRING(Name,1,3))
from displayString

--
Create table SwapNumbers
(
	[Row Id]  [Varchar] (1) NOT NULL
)
select * from SwapNumbers

Insert into SwapNumbers Values ('1');
Insert into SwapNumbers Values ('0');
Insert into SwapNumbers Values ('1');
Insert into SwapNumbers Values ('0');
Insert into SwapNumbers Values ('1');
Insert into SwapNumbers Values ('0');
Insert into SwapNumbers Values ('0');

--query to dispaly 1 insted of 0 and 0 instead of 1(Swap Numbers)

select [Row Id]
,case 
when [Row Id] =1 then '0'
else 
1
END AS TEST
FROM SwapNumbers

--Basics SQL
/*
 SELECT * FROM Customer WHERE lastname ='hardy'
 order by FirstName
  */

  select * from amit;


